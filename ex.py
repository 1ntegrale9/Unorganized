# coding: utf-8

#標準入力から以下のフォーマットで受け取る仕様
#ある時刻T 開始時刻S 終了時刻E

T,S,E = map(int,input().split()) #time,start,end

#result = True or False
if S == E:
  result = (S == T) #S = T ならば True
elif S <= E:
  result = ((S <= T) and (T < E)) #S <= T < E ならば True
else: #E <= S
  result = not ((E <= T) and (T < S)) #E <= T < S ならば False

#result = True ならば範囲内, result = Falseならば範囲外
print("時間の範囲内の時刻です" if result else "時間の範囲外の時刻です")