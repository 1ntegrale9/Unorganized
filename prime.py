def defineNumberOfUpperLimit():
    import sys

    try:
        upperLimitNumber = sys.argv[1]
    except IndexError:
        print("please enter the number of upper limit.")
        upperLimitNumber = input()

    try:
        return int(upperLimitNumber)
    except:
        print("Error: input value is not number.")
        sys.exit()

def createPrimeList(upperlimitNumber):
    primeList = [2]

    def isPrime(number):
        for prime in primeList:
            if number % prime == 0:
                return False
        return True

    number = 3
    while number < upperlimitNumber:
        if isPrime(number):
            primeList.append(number)
        number = number + 2

    return primeList

def main():
    print(createPrimeList(defineNumberOfUpperLimit()))

main()
