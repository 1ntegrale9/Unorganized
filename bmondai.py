def dp(st,en,through):
    through.append(st)
    try:
        if en in road[st]:
            return True
        else:
            for i in road[st]:
                if not i in through:
                    if dp(i,en,through) == True:
                        return True
            return False
    except KeyError:
        return False

N,M,S = map(int,input().split())

road = {}
for i in range(M):
    road[i+1] = []

for i in range(M):
    ui,vi = map(int,input().split())
    road[ui].append(vi)
    road[vi].append(ui)

through = []
if S != 1:
    print(1)
    through.append(1)
    for i in range(2,S,1):
        if dp(S,i,through[:]) == True:
            print(i)
            through.append(i)
print(S)