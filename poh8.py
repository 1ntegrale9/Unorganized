ショートヘア
>|python|
#繰り返しの回数
RepeatNum = int(input())
#出力する文字列
String = input()

for C in range(RepeatNum):
    print(String)
||<

ロングヘア
>|python|
#正の整数 N
N = int(input())

Answer = "lucky" if N % 7 == 0 else "unlucky"
print(Answer)
||<

ポニーテール
>|python|
NoCA = 0 #number of correct answers
for n in range(5):
    dn,en = input().split()
    if dn == en:
        NoCA += 1

Answer = "OK" if NoCA >= 3 else "NG"
print(Answer)
||<

ツインテール
>|python|
#全体の長さ
TotalLength = int(input())
#現在の進捗状況の値
CurrentStatus = int(input())

CharList = ["-" for C in range(TotalLength)]
CurrentIndex = CurrentStatus - 1
CharList[CurrentIndex] = "+"
Answer = "".join(CharList)
print(Answer)
||<

おさげ
>|python|
def Check(MaxLength,MaxSongNum,SongNum,TotalLength):
    for n in range(MaxSongNum):
        SongLength = int(input())
        TotalLength += SongLength
        if TotalLength > MaxLength:
            return(SongNum)
        SongNum += 1
    return("OK")

#CDに入る曲の最大の長さ(sec)
MaxLength = int(input()) * 60
#収録したい曲数
MaxSongNum = int(input())

Answer = Check(MaxLength,MaxSongNum,0,0)
print(Answer)
||<

たれ目
>|python|
#映画館の残りの席数, グループの人数
RemainSeatsNum,PeopleNum = map(int,input().split())

Answer = "OK" if RemainSeatNum >= PeopleNum else "NG"
print(Answer)
||<

つり目
>|python|
#p は買物額を表します。
p = int(input())

Answer = p//100 if p < 1000 else p//100 + 10
print(Answer)
||<

めがね
>|python|
#数字の個数を表す N
N = int(input())
NumSequence = [i for i in map(int,input().split())].sort(reverse=True)

CenterIndex = (N+1)//2 -1
Answer = NumSequence[CenterIndex]
print(Answer)
||<

Cute衣装
>|python|
#n はスタッフの人数、m はパックに入っているアメ玉の数を表します。
n,m = map(int,input().split())

Answer = "ok" if m % n == 0 else "ng"
print(Answer)
||<

Sexy衣装
>|python|
#元の位置から進んだ歩数 M, 進んだ先から下がった歩数 N
M,N = map(int,input().split())

CP = M - N #CurrentPosition
Answer = CP if CP > 0 else 0
print(Answer)
||<

制服
>|python|
CardSequence = ["3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A", "2"]
OrderSequence = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
Rank = [0 for C in range(52)]
Hand = [OrderSequence[CardSequence.index(C)] for C in input().split()]

top,highest = 0,52
while hand.count(0) != 52:
    for i,e in enumerate(hand):
        if highest == i:
            top = 0
        elif e > top:
            top,highest = e,i
            hand[i] = 0
            rank[i] = max(rank) + 1

for i in rank:
    print(i)
||<

浴衣
>|python|
#冷蔵庫からの出し入れの回数を表す N
N = int(input())

eue2 = 0 #electric utility expense
temp = 0 #temperature
lasttime = 0
for i in range(N):
    ti,si = input().split()
    up = 5 if si == "in" else 3
    tdiff = int(ti) - lasttime
    eue2 += up
    temp += up - tdiff if temp - tdiff >= 0 else up - temp
    lasttime = int(ti)
temp -= 24 - lasttime

answer = 24-eue2 + 2*eue2 if temp <= 0 else 24-eue2 + 2*eue2 - temp
print(answer)
||<

水着
>|python|
#変更前の文字列の長さ n と 変更後の文字列の長さ m
n,m = map(int,input().split())
#変更前の文字列 s
s = input()
#変更後の文字列 t
t = input()

counter = {}
for c in s:
    if not c in counter:
        counter[c] = 1
    else:
        counter[c] += 1
for c in t:
    if not c in counter:
        counter[c] = -1
    else:
        counter[c] -= 1

answer = sum(-1*i for i in counter.values() if i < 0)
print(answer)
||<

マイク
>|python|
import math

#1日にこなせるイベント回数 n
n = int(input())
#計画されているイベント総回数 m
m = int(input())

answer = math.ceil(m/(n*2))
print(answer)
||<

カチューシャ
>|python|
import math

#ファンの人数 n 人、色紙1枚の費用 p 円
n,p = map(int,input().split())
#ペン1本でサインの書ける色紙枚数 m　枚、ペン1本の費用 q 円
m,q = map(int,input().split())

answer = math.ceil(n/m)*q + n*p
print(answer)
||<