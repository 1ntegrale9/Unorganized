# -*- coding: utf-8 -*-

#作成者：丹内駿
#最終更新日時：2014/07/27
#言語：python2

import math
import random
import numpy as np

#値をn乗する
def multiplier(value,n=2):
    for count in range(n-1):
        value *= value
    return value


#ユークリッドの互除法で最大公約数を求める
def Euclidean(a,b):
    if(a<b): a,b=b,a #a<bならばaとbを入れ替え
    if a==0 or b==0: return 1 #どちらかの値が0の場合
    else:
        while(True): #ユークリッドの互除法
            a %= b
            if a==0: return b
            a,b = b,a
#1.a/bの余りをaとする
#2.aが0の場合、bを最大公約数として終了
#3.aとbを入れ替えて1に戻る
#上の2つのif文はエラー処理なのでアルゴリズムとして記述する必要はない


#既約分数を求める
def irrtored(redfraction): #可約分数 reducible fraction
    redmol = redfraction[0] #可約分子 reducible molecule
    redden = redfraction[1] #可約分母 reducible denominator
    gcd = Euclidean(redmol,redden) #最大公約数 greatest common divisor
    irrmol = redmol / gcd #既約分子 irrducible molecule
    irrden = redden / gcd #既約分母 irrducible denominator
    irrfraction = [irrmol,irrden] #既約分数 irreducible fraction
    return irrfraction
#1.可約分数を読み込む
#2.ユークリッドの互除法で分子と分母に対して最大公約数を求める
#3.分子と分母を最大公約数で割る


#分数の和を求める
def addfract(addeE,addeR):
    molecule = addeE[0]*addeR[1] + addeR[0]*addeE[1] #分子
    denominator = addeE[1] * addeR[1] #分母
    return irrtored([molecule,denominator]) #既約分数にして返す
#1.2つの分数a,bを読み込む
#2.aの分母とbの分母の積を解の分母とする
#3.（aの分子とbの分母の積）と（bの分子とaの分母の積）の和を解の分子とする
#4.求めた解は可約分数なので既約分数にする


#重複判定
def checkoverlap(target,list):
    overlap = 0 #重複が見つからなければfalse
    for check in list: #リストの全ての要素を調べる
        if target == check: #要素と比較
            overlap = 1 #重複が見つかった場合true
    return overlap #真偽値を返す
#1.調べたい値とリストを読み込む
#2.リストの全ての要素と調べたい値を比較する
#3.重複が見つかれば判定はtrue、見つからなければfalse


#行列を表示する
def printmatrix(matrix):
    nrow = len(matrix) #行数
    ncol = len(matrix[0]) #列数
    for row in range(nrow):
        print matrix[row]
#プログラムの仕様なのでアルゴリズムを記述する必要はない


#行列の要素を実数から分数ににする
def matrixconvertfraction(matrix):
    nrow = len(matrix)
    ncol = len(matrix[0])
    for row in range(nrow):
        element = []
        for col in range(ncol):
            fraction = [] #分数を格納する配列
            fraction.append(matrix[row][col]) #要素を分子に
            fraction.append(1) #分母に1
            matrix[row][col] = fraction
#プログラムの仕様なのでアルゴリズムを記述する必要はない


#行列の要素を分数から実数にする
def matrixconvertreal(matrix):
    nrow = len(matrix)
    ncol = len(matrix[0])
    for row in range(nrow):
        for col in range(ncol):
            if(matrix[row][col][0] == 0):
                matrix[row][col] = 0
            else:
                matrix[row][col] = (matrix[row][col][0] * 1.0) / (matrix[row][col][1] * 1.0) #分数を実数に変換する
#プログラムの仕様なのでアルゴリズムを記述する必要はない

#行列の要素から虚数部を削除する
def matrixconvertabs(matrix):
    copy = []
    nrow = len(matrix)
    ncol = len(matrix[0])
    for row in range(nrow):
        element = []
        for col in range(ncol):
            element.append(matrix[row][col].real)
        copy.append(element)
    return copy
#プログラムの仕様なのでアルゴリズムを記述する必要はない


#行列をコピーする
def copymatrix(matrix):
    copy = []
    nrow = len(matrix)
    ncol = len(matrix[0])
    for row in range(nrow):
        element = []
        for col in range(ncol):
            element.append(matrix[row][col])
        copy.append(element)
    return copy
#プログラムの仕様なのでアルゴリズムを記述する必要はない

                           
#連立方程式の係数行列を正方行列と列ベクトルに分解する
def decsimequ(matrix): #decomposition simultaneous equations
    squarematrix = []
    vector = []
    for row in range(len(matrix)):
        melement = []
        for col in range(len(matrix[0])):
            if(col!=len(matrix[0])-1):
                melement.append(matrix[row][col])
            else:
                vector.append(matrix[row][col])
        squarematrix.append(melement)
    simequ = [squarematrix,vector]
    return simequ


#回転行列を生成する
def rotationmatrix(dimension,coordinate,theta):
    row = coordinate[0]
    col = coordinate[1]
    matrix = np.identity(dimension) #dimension次の単位行列を生成
    matrix[row][row] = matrix[col][col] = math.cos(theta)
    matrix[row][col] = -1.0 * math.sin(theta)
    matrix[col][row] = math.sin(theta)

    return matrix
#プログラムの仕様なのでアルゴリズムを記述する必要はない


#行列の非対角要素の最大値の座標を求める
def maxelement(matrix):
    maxel = []
    maxvalue = 0
    nrow = len(matrix)
    ncol = len(matrix[0])
    for row in range(nrow):
        for col in range(ncol):
            if(row != col): #非対角成分のとき
                if(math.fabs(maxvalue) <= math.fabs(matrix[row][col])): #絶対値を比較
                    maxel = [row,col] #最大値の座標を更新
    return maxel
#プログラムの仕様なのでアルゴリズムを記述する必要はない


#要素を入力させて行列を作成する
def matrixdef():
    matrix = [] #行列
    print "Please enter the number of row and columns of the matrix." #行列の行数と列数を入力してください
    print "number of row :" ,
    nrow = input() #行数
    print "number of col :" ,
    ncol = input() #列数

    print "Please enter the elements of the matrix." #行列の要素を入力してください
    for row in range(nrow): #全ての行
        element = [] #要素
        for col in range(ncol): #全ての列
            print "row : " + str(row) + " col : " + str(col) + " value =" , #row行col列の要素の値は？
            element.append(input()) #行列に入力した要素を追加
        matrix.append(element) #行を順に追加して行列を生成

    return matrix #行列を返す
#プログラムの仕様なのでアルゴリズムを記述する必要はない


#対称正方行列を生成する
def mirrormatrix(matrix):
    nrow = len(matrix)
    ncol = len(matrix[0])
    for row in range(nrow):
        for col in range(ncol):
            if(nrow>=ncol):
                matrix[row][col] = matrix[col][row]
#プログラムの仕様なのでアルゴリズムを記述する必要はない


#自動で行列を生成する
def matrixgenerate():
    matrix = [] #行列
    print "Please enter the number of row and columns of the matrix." #行列の行数と列数を入力してください
    print "number of row :" ,
    nrow = input() #行数
    print "number of col :" ,
    ncol = input() #列数

    for row in range(nrow):
        element = []
        for col in range(ncol):
            element.append(random.randint(0,10))
        matrix.append(element)

    return matrix #行列を返す
#プログラムの仕様なのでアルゴリズムを記述する必要はない


#余因子展開による行列式の計算 cofactor expansion
def cofexp(matrix,exclusion=[]):
    row = len(exclusion) #何行目
    if row < len(matrix): #途中の行ならば(最後の行ではないならば)
        sign = 1 #符号
        col = 0 #何列目
        result = 0 #計算結果
        while col < len(matrix[row]): #最後の列まで処理を行う
            if checkoverlap(col,exclusion) == 0: #同じ列の要素を演算に使用しない
                exclusion.append(col) #使用した要素の列を記録
                result += sign * matrix[row][col] * cofexp(matrix,exclusion) #余因子の和をとる
                exclusion.pop() #再帰のためここで最後の記録を削除
                sign *= -1 #順番に余因子の符号が反転
            col += 1 #次の列の処理へ
        return result
    else: #最後の行
        return 1
#1.行列を読み込む
#2.再帰関数cofexpを定義、選択した列の番号を格納する配列eを引数とする
#3.配列eの要素数をrとしてr行を見る
#4.r行が最後の行ならば1を返す
#5.変数signの値を1にする
#6.r行目の配列eに含まれない列の番号cを配列eに格納する
#7.変数resultにsignと(r行c列の値)とcofexp(e)の返り値を加算する
#8.配列eからcを削除し、signに-1を掛ける
#9.6~8を1列目から最後の列までの全ての場合について行う
#10.resultを返す


#ガウス・ジョルダン法による連立方程式の計算
def GaussJordan(matrix):
    nrow = len(matrix) #行数
    ncol = len(matrix[0]) #列数
    simeq = copymatrix(matrix) #連立方程式 simultaneous equations
    
    #行列の要素を分数にする
    matrixconvertfraction(simeq)

    #行列にガウス・ジョルダン法を適用する
    for target in range(nrow): #target:係数を1にする変数の順番号
        
        #係数を1にする変数の係数の逆数
        recmol = simeq[target][target][1] #逆数の分子 Reciprocal Molecule
        recden = simeq[target][target][0] #逆数の分母 Reciprocal Denominator
        
        #target行に逆数を掛ける
        for col in range(ncol):
            #target行のcol列に逆数を掛ける
            simeq[target][col][0] *= recmol
            simeq[target][col][1] *= recden
            simeq[target][col] = irrtored(simeq[target][col]) #既約分数にする
        
        #target行以外のtarget変数の係数を0にする
        for row in range(nrow):
            if(row != target):
                varmol = simeq[row][target][0] #0にする係数の分子
                varden = simeq[row][target][1] #0にする係数の分母
                for col in range(ncol): #-m/d倍して加算(減算)を行いtarget変数の係数を0にする
                    addeE = simeq[row][col]
                    addeR = []
                    addeR.append(simeq[target][col][0] * varmol * -1)
                    addeR.append(simeq[target][col][1] * varden)
                    simeq[row][col] = addfract(addeE,addeR)
            
    #行列の要素を実数にする
    matrixconvertreal(simeq)
    
    result = []
    for row in range(nrow):
        result.append(simeq[row][ncol-1])
    return result
#1.連立方程式の係数行列を読み込む
#2.係数行列の行数をr、列数をcとする
#2.n行n列の値の逆数をn行の両辺に掛ける（n行n列の値を1にする）
#3.m行に対して、n行p列の値と-1×(m行n列の値)/(n行n列の値)の積をm行p列の値に加算する。pが1からcまでのすべての場合について加算を行う。
#4.mが1からrまでのnを除いたすべての場合についてこの3の操作を行う。
#5.nが1からrまでの全ての場合について2~4をの操作を行う。
#分数,実数への変換はプログラムの仕様なのでアルゴリズムとして記述する必要はない


#Jacobi法によって固有値と固有ベクトルを求める
def eigen(matrix):
    dimension = len(matrix) #次数
    eigenmatrix = copymatrix(matrix) #固有値行列 Eigenvalue matrix
    orthmatrix = np.identity(dimension) #直交行列 orthogonal matrix
    
    for i in range(100000):
        #非対角成分の最大値(座標)を求める
        maxel = maxelement(eigenmatrix) #最大値(の座標) max element
        row = maxel[0]
        col = maxel[1]
        err = eigenmatrix[row][row] * 1.0
        ecc = eigenmatrix[col][col] * 1.0
        erc = eigenmatrix[row][col] * 1.0

        #回転角を求める
        if(int(err)!=int(ecc)):
            theta = 0.5 * math.atan(2.0*erc/(err-ecc))
        else:
            theta = math.pi / 4.0
    
        #回転行列を求める
        rotmatrix = copymatrix(rotationmatrix(dimension,maxel,theta))

        #直交行列と回転行列の積を新たな直交行列とする
        orthmatrix = copymatrix(np.dot(orthmatrix,rotmatrix))

        #回転行列の転置と固有値行列と回転行列の積を新たな固有値行列とする
        eigenmatrix = copymatrix(np.dot(np.transpose(rotmatrix),np.dot(eigenmatrix,rotmatrix)))

    eigenmatrixp = copymatrix(eigenmatrix)
    orthmatrixp = copymatrix(orthmatrix)
    print "\n" + "eigenvalue"
    printmatrix(eigenmatrixp)
    print "\n" + "eigenvector"
    printmatrix(orthmatrixp)


#メニュー
def menu(matrix=[]):
    print ""
    print "Please select the operation."
    print "1 : generate matrix manually"
    print "2 : generate matrix automatic"
    print "3 : calculate the determinant"
    print "4 : calculate the solution of simultaneous equations"
    print "5 : calculate the eigenvalue and eigenvector"
    print "6 : exit"
    print ""
    print "operation : ",
    value = input()
    if value == 1:
        matrix = matrixdef() #行列を手動で生成
        print "\n" + "matrix is ..."
        printmatrix(matrix)
    if value == 2:
        matrix = matrixgenerate() #行列を自動で生成
        mirrormatrix(matrix)
        print "\n" + "matrix is ..."
        printmatrix(matrix)
    if value == 3:
        if matrix == []:
            print "Matrix does not exist." #行列がありません
        elif len(matrix) != len(matrix[0]):
            print "Matrix is not a square." #正方行列ではありません
        else:
            det = cofexp(matrix) #行列を引数にして行列式を計算
            print "determinant(calculate by implementation  ) : ",
            print str(det*1.0)
            print "determinant(calculate by module in python) : ",
            print np.linalg.det(matrix)
    if value == 4:
        if matrix == []:
            print "Matrix does not exist." #行列がありません
        elif len(matrix) != len(matrix[0]) - 1:
            print "Matrix is not simultaneous equations." #連立方程式ではありません
        else:
            solution = GaussJordan(matrix) #行列を引数にして連立方程式の解を求める
            print "\n" + "solution(caluculate by implementation)"
            for num in range(len(solution)):
                print "Variable No." + str(num+1) + " = " + str(solution[num])
            simequ = decsimequ(matrix)
            solution = np.linalg.solve(simequ[0],simequ[1])
            print "\n" + "solution(caluculate by module in python)"
            for num in range(len(solution)):
                print "Variable No." + str(num+1) + " = " + str(solution[num])
    if value == 5:
        eigenvalue,eigenvector = np.linalg.eig(matrix)
        print "\n" + "eigenvalue(calculate by module in python)"
        tmpvector = []
        for num in range(len(eigenvalue)):
            tmpvector.append(eigenvalue[num].real)
        print tmpvector
        print "\n" + "eigenvector(calculate by module in python)"
        printmatrix(matrixconvertabs(eigenvector))
    if value == 6:
        exit()
    else:
        menu(matrix)

menu()