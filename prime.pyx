import time
import sys

def cprime(int n):
	cdef int num, it
	notprime = [it for it in range(3,n,2)]

	prime = [2]
	while len(notprime) != 0:
		num = notprime[0]
		notprime = [it for it in notprime if it%num != 0]
		prime.append(num)
	return(prime)