import prime
import time
import sys

start = time.time()
p = prime.cprime(1000000)
end = time.time()
print(("time: {0}".format(end - start)) + "[sec]")