#! /usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import print_function
from janome.tokenizer import Tokenizer
from gensim.models import word2vec
from tqdm import tqdm
import xml.etree.ElementTree as ET
import sys
import MeCab
import time
import subprocess
import unicodedata
import csv
import fileinput
import locale
import sys
import os
import json
import re
import math

# 文字コード判定
def simple_chardet(b_str):
    try:
        b_str.decode ('iso-2022-jp')
        return '7bit (ascii, iso-2022-jp, etc...)'
    except UnicodeDecodeError:
        try:
            b_str.decode ('utf-8')
            return('utf-8')
        except UnicodeDecodeError:
            try:
                b_str.decode ('cp932')
                return 'cp932'
            except UnicodeDecodeError:
                try:
                    b_str.decode ('eucjp')
                    return 'eucjp'
                except UnicodeDecodeError:
                    return None

# 文字コード出力
def printchardet(infile):
    locale.setlocale (locale.LC_ALL, '')
    maxsize = 1 * 1024 * 1024    # 最大で1MiBまで読み込むことにする

    try:
        with open (infile, 'rb') as f_in:
            # 内容を読み込む
            try:
                b_text = f_in.read (maxsize)
            except IOError as e:
                sys.exit ('Error: cannot read from file "{0}" [{1}]: {2}'.format (infile, e.errno, e.strerror))
    except IOError as e:
        sys.exit ('Error: cannot open file "{0}" [{1}]: {2}'.format (infile, e.errno, e.strerror))

    # エンコーディング検出を実行
    encoding = simple_chardet (b_text)

    # 結果の表示
    # encodingがNoneでなければencodingの内容を表示し
    # Noneであれば「other encoding」と表示
    print (encoding if encoding else 'other encoding')

# 記号削除
def delsym(text):
    return(re.sub("[︰-＠]","",re.sub("[!-~]","",text)))

# 正規化
def TextNormalize(text):
    return(unicodedata.normalize('NFKC',text))

# データ読み込み
def fileinput(filename,encoding):
    with open(filename,encoding=encoding) as file:
        return(file.read())

#jsonデータ読み込み
def jfileinput(filename,encoding):
    with open(filename,'r',encoding=encoding) as file:
        return(json.load(file))

#jsonデータ上書き
def jfileoutput(filename,encoding,data):
    with open(filename,'w',encoding=encoding) as file:
        json.dump(data,file,sort_keys = True, indent = 4)

# 空白と改行の除去
def BlankRemove(text):
    return("".join(text.split()))

# 文章分解
def SentenceDisassembly(text):
    Process1 = text.rstrip('。')
    Process2 = Process1.rstrip('.')
    Process3 = Process2.split('。')
    Process4 = ".".join(Process3)
    Process5 = Process4.split('.')
    return(Process5)

# 文字列を1セクションずつの配列に
def TextToArray(text,splitchar):
    array = [TextNormalize(e) for e in text.split(splitchar)]
    return(array)

# 文字列を1行ずつの配列に
def TextLineToArray(text):
    array = [i for i in text.splitlines()]
    return(array)

# データ分解
def DataDisassembly(text):
    head,body,arraylist = [],[],[]
    for line in text:
        array = []
        for nc in range(len(line)):
            if nc == 0:
                head.append(line[nc])
            elif nc == 1:
                body.append(line[nc])
            else:
                array.append(line[nc])
        arraylist.append(array)
    return(head,body,arraylist)

# 形態素解析(janome) return(3重配列[全文[行[形態素]]])
def JMorphologicalAnalysis(text):
    tokens = Tokenizer().tokenize(text)
    array = [",".join(str(token).split()).split(",") for token in tokens]
    return(array)

# 形態素解析(MeCab) return(3重配列[全文[行[形態素]]])
def MorphologicalAnalysis(text):
    tokens = MeCab.Tagger('-d /usr/local/lib/mecab/dic/mecab-ipadic-neologd').parse(text)
    array = [",".join(str(token).split()).split(",") for token in tokens.split("\n")]
    return(array[:len(array)-2])

# 名詞抽出 return(2重配列[全文[名詞列]])
def NounExtraction(morpheme):
    array = [it[0] for it in morpheme if it[1] == "名詞"]
    return(array)

# 単語抽出 return(2重配列[全文[単語列]])
def TermExtraction(morpheme):
    array = [it[0] for it in morpheme if it[1] == "名詞"]
    return(array)

# ファイルパス
def filepass(filename):
    a,b,c = filename[4],filename[5],filename[6]
    return('newspaper/199'+str(a)+'/'+str(b)+str(c)+'/'+str(filename))

# cos類似度
def cos(v1, v2):
    numerator = sum([v1[c] * v2[c] for c in v1 if c in v2])
    denominator =  math.sqrt(sum([v * v for v in v1.values()]) * sum([v * v for v in v2.values()]))
    return float(numerator) / denominator if denominator != 0 else 0

# 類似度計算
def similarity(model,dic,posi, nega=[], n=10000):
        result = model.most_similar(positive = posi, negative = nega, topn = n)
        return([r[0] for r in result if r[0] in dic])

def s(posi, nega=[], n=100):
    cnt = 1
    result = model.most_similar(positive = posi, negative = nega, topn = n)
    for r in result:
        print(cnt, r[0], r[1])
        cnt += 1

# QAシステム
def nlp1():
    inputfile  = 'formalrun_query.txt'
    outputfile = 'formalrun_answer.txt'
    QInput = fileinput(inputfile,'euc-jp')
    QData = [TextToArray(it,",") for it in TextLineToArray(QInput)]
    Qnum,question,ansnumlist = DataDisassembly(QData)
    #janome = [MorphologicalAnalysis(it) for it in question]
    mecab = [MorphologicalAnalysis(it) for it in question]
    query = [TermExtraction(it) for it in mecab]

    #WordEmbbedingの読み込み
    bincorpus = "newspaper_size200_cbow_window8_negative25.bin"
    model = word2vec.Word2Vec.load_word2vec_format(bincorpus, binary=True)

    with open(outputfile, "w",encoding='eucjp') as file:
        for index,ansnumline in enumerate(tqdm(ansnumlist)):
            file.write(str(Qnum[index]))
            sym = ""
            for ansnum in ansnumline:
                filename = 'JA-'+str(ansnum)+'.txt'
                filep    = filepass(filename)
                if filename in os.listdir(filep[0:17]):
                    Nounlines = TermExtraction(MorphologicalAnalysis(TextNormalize(BlankRemove(fileinput(filep,'utf-8')))))
                    try:
                        NounsCandidacy = similarity(model,Nounlines,query[index])
                        for it in range(5):
                            try:
                                file.write(',"'+NounsCandidacy[it]+'",'+str(ansnum)+',,')
                            except IndexError:
                                file.write(',"'+"IndexError"+'",'+str(ansnum)+',,')
                    except KeyError:
                        file.write(',"'+"KeyError"+'",'+str(ansnum)+',,')
                else:
                    file.write(',"'+"None"+'",'+str(ansnum)+',,')
            file.write('\n')

def a(text):
    q = [it[0] for it in JMorphologicalAnalysis(text)]
    answer = model.n_similarity(query,q)
    print(answer)

# icd-10コード表 csv -> icd-10コード & 疾病名 json辞書
def icd():
    icd = {}
    for i in TextLineToArray(fileinput("icd10.csv","utf-8")):
        tmp = i.split(",")
        icd[tmp[1]] = tmp[2]
    jfileoutput("icd10.json","UTF-8",icd)

# 診療データ(完全) XML -> id & icd-10コード & 疾病名 json辞書
# id200~281,261は診断なし,273は存在しない
def ntcir11():
    dic = {}
    flag = 0 #261に空データを付与するための操作フラグ
    for i in TextLineToArray(fileinput("ntcir11_mednlp_mednlp2sub.xml","utf-8")):
        if i[0:8] == "<text id":
            if flag == 1:
                dic[id] = {}
            id = i.split("<text id=\"")[1].split("\" type=\"")[0]
            flag = 1
        elif i[0:2] == "診断":
            icd = {}
            for j in i.split("c icd=\"")[1:]:
                tmp = j.split("\">")
                code,name = tmp[0],tmp[1].split("</c>")[0]
                icd[code] = name
                dic[id] = icd
                flag = 0
    jfileoutput("ntcir11.json","UTF-8",dic)

# 診療データ(文章) txt -> id & 文字列 json辞書
def med():
    med = {}
    for i in fileinput("med.txt","UTF-8").split("</text>")[:-1]:
        id,text = i.split("id=\"")[1].split("\">")
        med[id] = delsym(text.replace("\n","").replace("現病歴","").replace("既往歴",""))
    jfileoutput("med.json","UTF-8",med)

def nlp2():
    # 各ファイルの読み込み
    med = jfileinput("med.json","UTF-8")            #診療データ
    ntcir11 = jfileinput("ntcir11.json","UTF-8")    #解答セット
    icd10 = jfileinput("icd10.json","UTF-8")        #疾病データ

    # WordEmbbedingの読み込み
    bincorpus = "newspaper_size200_cbow_window8_negative25.bin"
    model = word2vec.Word2Vec.load_word2vec_format(bincorpus, binary=True)

    for i in sorted(med.keys()):
        med[i]

    #tokens = Tokenizer().tokenize(text)

nlp2()

#inputfile,outputfile = 'sample.txt','sample.txt'
#Input = fileinput(inputfile,'utf-8')
#Data = [TextToArray(it,",") for it in TextLineToArray(Input)]
#mecab = JMorphologicalAnalysis(Input)
#query = [it[0] for it in mecab]

#bincorpus = "newspaper_size200_cbow_window8_negative25.bin"
#model = word2vec.Word2Vec.load_word2vec_format(bincorpus, binary=True)
#a("食道癌")
#a("誤嚥性肺炎")
#a("進行食道癌")
#a("食道静脈瘤破裂")
#a("上行結腸癌")
#a("脊柱管狭窄症")
#a("直腸癌")
#s(query)
