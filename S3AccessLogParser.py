# coding:utf-8

# 指定したディレクトリ内のS3アクセスログファイルをリスト化して出力
from __future__ import print_function
import os
import sys
import locale
import codecs
import traceback

S3AccessLogRecordFieldDic = {'BucketOwner':0, 'Bucket':1, 'Time1':2, 'Time2':3, 'RemoteIP':4, 'Requester':5, 'RequestID':6, 'Operation':7, 'Key':8, 'RequestURI1':9, 'RequestURI2':10, 'RequestURI3':11, 'HTTPstatus':12, 'Errorcode':13, 'BytesSent':14, 'ObjectSize':15, 'TotalTime':16, 'TurnAroundTime':17, 'Referrer':18, 'UserAgent':19, 'VersionId':20}

# 文字コードを簡単に検出する
def SimpleCharDet(b_str):
    def tryDecode(b_str, c_code):
        try:
            b_str.decode(c_code)
            return c_code
        except UnicodeDecodeError:
            return 'UDError'
    c_code_list = ['us-ascii', 'utf-8', 'cp932', 'eucjp']
    for c_code in c_code_list:
        if tryDecode(b_str, c_code) != c_code_list:
            return c_code
    return 'unknown'

# ファイルの文字コードを取得する
def GetCharCodeOf(inputFile):
    locale.setlocale(locale.LC_ALL, '')
    maxsize = 1 * 1024 * 1024
    def sysExit(e, errorMsg):
        sys.exit("Error: {4} '{0}' [{1}]: {2}".format(inputFile, e.errno, e.strerror, errorMsg))
    def tryReading(f_in):
        try:
            return f_in.read(maxsize)
        except IOError as e:
            sysExit(e, 'cannot read from file')
    def tryOpening():
        try:
            with open(inputFile, 'rb') as f_in:
                return tryReading(f_in)
        except IOError as e:
            sysExit(e, 'cannot open file')
    return SimpleCharDet(tryOpening())

# データ読み込み
def FileInput(filename, encoding):
    with codecs.open(filename, mode = 'r', encoding = encoding) as file:
        return file.read().encode('utf-8')

# ファイルのテキストを分解してリストにする
def TextSplitOf(filePath):
    textFilterOption = True
    fileCharCode = GetCharCodeOf(filePath)
    if fileCharCode != 'unknown':
        return [singleLine.split() for singleLine in FileInput(filePath, fileCharCode).splitlines() if textFilterOption]
    else:
        return 'error'

# 第一引数or標準入力でディレクトリパスを受け取る
def getDirPath():
    try:
        return sys.argv[1]
    except:
        print('Please enter the directory path.')
        return input()

# リストの出力に使うフィルタ
def S3LogFilterOption(targetLog, selectField, fieldValue):
    if S3AccessLogRecordFieldDic[selectField] == 0:
        return True
    else:
        return targetLog[S3AccessLogRecordFieldDic[selectField]] == fieldValue

# ディレクトリ内のログをリストで出力
def S3LogOutput(dirPath, showField, selectField, fieldValue):
    for fileName in os.listdir(dirPath):
        splitText = TextSplitOf(dirPath + fileName)
        if len(splitText) > 0:
            correspondingLog = [line for line in splitText if S3LogFilterOption(line, selectField, fieldValue)]
            if len(correspondingLog) > 0:
                print(fileName, end = '  ')
                print(showField, end = '  ')
                for log in correspondingLog:
                    if showField == 'All':
                        print(log, end = '  ')
                    else:
                        print(log[S3AccessLogRecordFieldDic[showField]], end = '  ')
                print('')

try:
    S3LogOutput(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
except:
    traceback.print_exc()
    print('directory path')
    dirPath = input()
    print('show field')
    showField = input()
    print('select field')
    selectField = input()
    print('field value')
    fieldValue = input()
    S3LogOutput(dirPath, showField, selectField, fieldValue)