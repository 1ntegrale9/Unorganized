import time
import sys

def p2(n):
    notprime = [i for i in range(3,n,2)]

    prime = [2]
    while len(notprime) != 0:
        num = notprime[0]
        notprime = [itr for itr in notprime if itr%num != 0]
        prime.append(num)
    """
        tmp = num/n * 100
        sys.stdout.write("\r")
        sys.stdout.write("{0:f}%".format(tmp))
        sys.stdout.flush()
    sys.stdout.write("\n")
    """
    return(prime)

start = time.time()
p = p2(100000)
elapsed_time = time.time() - start
print(("time: {0}".format(elapsed_time)) + "[sec]")