#coding:utf-8
import random#ランダムモジュール読み込み
import functools
from operator import add
r = range

def PitchMatch(SimilarityPhrase,PitchMatrix):#音高一致スコア関数定義[f() += 3]
    if len(SimilarityPhrase) == 1:#1回目の一致
        return 1
    else:#2回目以降の一致
        return PitchMatrix[SimilarityPhrase[len(SimilarityPhrase)-2][0]][SimilarityPhrase[len(SimilarityPhrase)-2][1]][2]+3

def PitchMissMatch(SimilarityPhrase,PitchMatrix):#音高不一致スコア関数定義[f() += -5]
    if len(SimilarityPhrase) == 1:#1回目の一致
        return 0
    else:#2回目以降の一致
        return PitchMatrix[SimilarityPhrase[len(SimilarityPhrase)-2][0]][SimilarityPhrase[len(SimilarityPhrase)-2][1]][2]-5

def ValueMatch():#音価一致スコア関数定義[f() = 2]
    return 2

def ValueMissMatch():#音価不一致スコア関数定義[f() = -1]
    return -1

def LAThreshold():#LA閾値関数定義[f() = 5]
    return 5

def GAThreshold():#GA閾値関数定義[f() = 0]
    return 0

def PhraseSimilarityScore(n,LAAllPhrase,SimilarityScore):#類似楽句対スコア関数定義
    LAAllPhrase[n][0] = LAAllPhrase[n][0] * SimilarityScore

def LocalAlignment(n1,n2,MaxScore,SimilarityPhrase,PitchMatrix):#ローカルアライメント関数定義
    if len(SimilarityPhrase) > 0:#初回実行エラー防止
        if SimilarityPhrase[len(SimilarityPhrase)-1] == [n1,n2]:#戻り時の重複を防ぐ
            SimilarityPhrase.remove([n1,n2])
    SimilarityPhrase.append([n1,n2])#通過点リストを更新
    PitchMatrix[n1][n2][3] = 1#通過記録
    p1 = 0 #縦の音高
    p2 = 0 #横の音高
    for d in r(len(SimilarityPhrase)):#現地点の音高を計算depth
        p1 += PitchMatrix[SimilarityPhrase[d][0]][SimilarityPhrase[d][1]][0]#縦の音高
        if d != 0:
            if PitchMatrix[SimilarityPhrase[d][0]][SimilarityPhrase[d][1]][0] == PitchMatrix[SimilarityPhrase[d-1][0]][SimilarityPhrase[d-1][1]][0]:
                p1 -= PitchMatrix[SimilarityPhrase[d][0]][SimilarityPhrase[d][1]][0]
        p2 += PitchMatrix[SimilarityPhrase[d][0]][SimilarityPhrase[d][1]][1]#横の音高
        if d != 0:
            if PitchMatrix[SimilarityPhrase[d][0]][SimilarityPhrase[d][1]][1] == PitchMatrix[SimilarityPhrase[d-1][0]][SimilarityPhrase[d-1][1]][1]:
                p1 -= PitchMatrix[SimilarityPhrase[d][0]][SimilarityPhrase[d][1]][1]
    if p1 == p2:#音高が一致の場合のスコア更新
        PitchMatrix[n1][n2][2] = PitchMatch(SimilarityPhrase,PitchMatrix)#一致スコア
        if PitchMatrix[n1][n2][2] > MaxScore:#現スコアが最高スコアより大きい
            MaxScore = PitchMatrix[n1][n2][2]#最高スコアを更新
    else:#音高が不一致の場合のスコア更新
        PitchMatrix[n1][n2][2] = PitchMissMatch(SimilarityPhrase,PitchMatrix)#不一致スコア
    if PitchMatrix[n1][n2][2] <= 0:#低スコアによる探索停止
        return back(n1,n2,MaxScore,SimilarityPhrase,PitchMatrix)#戻る
    else:
        if (n1 != length1-2) and (n2 != length2-2) and (PitchMatrix[n1+1][n2+1][3] == 0):#進行判定
            return LocalAlignment(n1+1,n2+1,MaxScore,SimilarityPhrase,PitchMatrix)#右下へ進む
        elif (n1 != length1-2) and (PitchMatrix[n1+1][n2][3] == 0):#進行判定
            return LocalAlignment(n1+1,n2,MaxScore,SimilarityPhrase,PitchMatrix)#下へ進む
        elif (n2 != length2-2) and (PitchMatrix[n1][n2+1][3] == 0):#進行判定
            return LocalAlignment(n1,n2+1,MaxScore,SimilarityPhrase,PitchMatrix)#右へ進む
        else:
            if PitchMatrix[n1][n2][2] == MaxScore:#最高スコア点で行き止まりの場合
                PitchMatrix[n1][n2][3] = 3#終了印
            return back(n1,n2,MaxScore,SimilarityPhrase,PitchMatrix)#戻る

def GlobalAlignment(n,LAAllPhrase):#グローバルアライメント関数定義
    smat = []
    for n1 in r(len(LAAllPhrase[n][1])+1):#縦+外枠
        stmp = []
        for n2 in r(len(LAAllPhrase[n][1])+1):#横+外枠
            if n1 == 0:#上側
                if n2 == 0:#左上
                    stmp.append(0)
                stmp.append(stmp[n2-1]+ValueMissMatch())
            elif n2 == 0:#左側
                stmp.append(smat[n1-1][0]+ValueMissMatch())
            else:
                if value1_[LAAllPhrase[n][1][n1-1][0]] == value2_[LAAllPhrase[n][1][n2-1][1]]:
                    slanting = smat[n1-1][n2-1]+ValueMatch()#斜め一致スコア = 左上スコア + 一致スコア
                else:
                    slanting = smat[n1-1][n2-1]#斜め不一致スコア = 左上スコア
                below = smat[n1-1][n2-1]#下スコア = 上スコア + 不一致スコア
                right = smat[n1-1][n2-1]#右スコア = 左スコア + 不一致スコア
                if (slanting >= below) and (slanting >= right):#斜めスコアが最大
                    stmp.append(slanting)#斜めスコアを付与
                elif (below > right):#下スコアが最大
                    stmp.append(below)#下スコアを付与
                else:#右スコアが最大または右スコア=下スコア
                    stmp.append(right)#右スコアを付与
        smat.append(stmp)
    return smat[n1][n2]#GAスコア(=右下スコア)を戻す

def back(n1,n2,MaxScore,SimilarityPhrase,PitchMatrix):#LA枝刈り関数定義
    if PitchMatrix[n1][n2][2] < MaxScore:
        PitchMatrix[n1][n2][2] = 0#点数をリセット
        PitchMatrix[n1][n2][3] = 2#停止記録
        SimilarityPhrase.remove([n1,n2])#通過点リストから削除
        return LocalAlignment(SimilarityPhrase[len(SimilarityPhrase)-1][0],SimilarityPhrase[len(SimilarityPhrase)-1][1],MaxScore,SimilarityPhrase,PitchMatrix)
    elif PitchMatrix[n1][n2][3] != 3:#終了判定
        return LocalAlignment(SimilarityPhrase[len(SimilarityPhrase)-1][0],SimilarityPhrase[len(SimilarityPhrase)-1][1],MaxScore,SimilarityPhrase,PitchMatrix)#最高スコア点から別方向へ
    else:#la終了
        PitchMatrix[n1][n2][3] = 0
        for i in r(length1-1):#停止印を削除
            for j in r(length2-1):
                if PitchMatrix[i][j][3] == 2:
                    PitchMatrix[i][j][3] = 0
        return [MaxScore,SimilarityPhrase,PitchMatrix]

def SongArrayGenerate(length):
    pitch  = [random.randint(1,10) for times in r(length)] #ランダム音高情配列
    value  = [random.choice((1.0,2.0,4.0,8.0,16.0,32.0)) for i in r(length)] #ランダム音価配列
    pitch_ = [pitch[ptr+1]-pitch[ptr] for ptr in r(length-1)] #音高差配列生成
    value_ = [value[ptr+1]/value[ptr] for ptr in r(length-1)] #音価比配列生成
    return(pitch,pitch_,value,value_)

#旋律生成
length1,length2 = [random.randint(100,200) for times in range(2)] #配列の長さをランダムで決める
pitch1,pitch1_,value1,value1_ = SongArrayGenerate(length1) #旋律1
pitch2,pitch2_,value2,value2_ = SongArrayGenerate(length2) #旋律2

#ローカルアライメント
PitchMatrix = [[[pitch1_[n1],pitch2_[n2],0,0] for n2 in r(length2-1)] for n1 in r(length1-1)] #行列を初期化[音高差1,音高差2,スコア,判定]
LAAllPhrase = []#類似楽句組を格納する配列all phraze
for n1 in r(length1-1):
    for n2 in r(length2-1):
        if pitch1_[n1] == pitch2_[n2]:#音高が一致した場合
            MaxScore = 0#最大点数を入れる変数max score
            SimilarityPhrase = []#類似楽句組を入れる配列similarity phrase
            if PitchMatrix[n1][n2][3] == 0:#類似楽句組に使われてない音の場合
                dat = LocalAlignment(n1,n2,MaxScore,SimilarityPhrase,PitchMatrix)#類似楽句組の情報[MaxScore,SimilarityPhrase,dp]を格納
                if dat[0] > LAThreshold():#スコアが閾値を超えている場合
                    LAAllPhrase.append(dat)#類似楽句組の情報[MaxScore,SimilarityPhrase,dp]を格納
                PitchMatrix = dat[2]#行列を更新

#グローバルアライメント
GAAllPhrase = []#類似楽句対を格納する配列all phrase
for n in r(len(LAAllPhrase)):
    PitchSimilarytyScore = GlobalAlignment(n,LAAllPhrase)#音価類似度
    PhraseSimilarityScore(n,LAAllPhrase,PitchSimilarytyScore)#類似楽句対類似度計算
    if PitchSimilarytyScore > GAThreshold():#音価類似度が閾値を超えている場合
        GAAllPhrase.append(LAAllPhrase[n])#類似楽句対を格納

#類似度計算
total  = functools.reduce(add,[GAPhrase[0] for GAPhrase in GAAllPhrase])

#情報出力
print('')
print('>>melody 1')
print('length = ' + str(length1))
print('pitch = '  + str(pitch1))
print('value = '  + str(value1))
print('')
print('>>melody 2')
print('length = ' + str(length2))
print('pitch = '  + str(pitch2))
print('value = '  + str(value2))
print('')
print('>>similarity phraze')
if len(GAAllPhrase) == 0:#類似楽句組がないとき
    print('None')
else:
    for i in r(len(GAAllPhrase)):
        print('score = '+str(GAAllPhrase[i][0]))
        Phrase1Number,Phrase2Number,Phrase1Pitchs,Phrase2Pitchs = [],[],[],[]
        for j in r(len(GAAllPhrase[i][1])):
            Phrase1Number.append(GAAllPhrase[i][1][j][0])
            Phrase2Number.append(GAAllPhrase[i][1][j][1])
            Phrase1Pitchs.append(pitch1[GAAllPhrase[i][1][j][0]])
            Phrase2Pitchs.append(pitch2[GAAllPhrase[i][1][j][1]])
        print('phrase1 number ' + str(Phrase1Number))
        print('phrase1 pitchs ' + str(Phrase1Pitchs))
        print('phrase2 pitchs ' + str(Phrase2Pitchs))
        print('phrase2 number ' + str(Phrase2Number))
        print('')

print('')
print('>>PhraseSimilarityScore(total score)')
print(total)
print('')