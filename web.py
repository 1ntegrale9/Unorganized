import re
from html.parser import HTMLParser
from urllib.request import urlopen

"""
class MyHTMLParser(HTMLParser):
  def handle_starttag(self, tag, attrs):
    if tag == 'a':
      print(dict(attrs).get('href'))
"""

#url = 'http://www.bing.com/search?q=%E5%A4%8F%E7%9B%AE%E6%BC%B1%E7%9F%B3'
url = 'http://qiita.com/'
f = urlopen(url)
parser = HTMLParser()
result = parser.feed(f.read().decode(f.info().get_content_charset()))
print(result)

pattern = r"http"
urls = []
for i in str(result).splitlines():
  matchOB = re.match(pattern,i)
  if matchOB:
    urls.append(i)

#print(urls)