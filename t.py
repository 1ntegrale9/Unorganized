import time
import sys

for i in range(1, 101):
    time.sleep(0.01)
    sys.stdout.write("\r")
    sys.stdout.write("{0}%".format(i))
    sys.stdout.flush()
sys.stdout.write("\n")